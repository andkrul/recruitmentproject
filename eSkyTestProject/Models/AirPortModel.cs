﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eSkyTestProject.Models
{
    public class Properties
    {
        public string cityName { get; set; }
        public string airportName { get; set; }
        public string countryName { get; set; }
        public string airportCode { get; set; }
    }

    public class Result
    {
        public string cumulatus { get; set; }
        public int ord { get; set; }
        public string cumulatus_strict { get; set; }
        public Properties properties { get; set; }
        public string type { get; set; }
        public string suggestionType { get; set; }
        public string code { get; set; }
        public string countryname { get; set; }
        public string regioncode { get; set; }
        public string suggestion { get; set; }
        public string coordinates { get; set; }
        public double score { get; set; }
        public string suggestionWithTags { get; set; }
    }

    public class AirPortModel
    {
        public List<Result> result { get; set; }
    }
}
