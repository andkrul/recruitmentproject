﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eSkyTestProject
{
    public class EnvironmentVariables
    {
        public static string AirporAutoCompleteSerivceURL => ConfigurationManager.AppSettings["AirporAutoCompleteSerivceURL"];
        public static string BaseURL => ConfigurationManager.AppSettings["BaseURL"];
        
    }
}
