﻿@UITests
Feature: UIAutomation
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers



Scenario: Searching for flyight
	Given I am on main Page
	And I have entered kraków into the  departure input
	And I have entered berlin into the  arrival input
	And I have entered 16 Wrzesień 2016 into the departure date input
	And I have entered 26 Listopad 2016 into the arrival date input
	When I hit search button	
	Then Flight should be displayed


Scenario: Searching for information abouit sausage
	Given I am on main Page
	When I click on Kontakt link
	And I chose Bagaż from left pane
	And I chose  sausage from right pane
	Then I am on page with information about sausage

Scenario: Szukanie hotelu 
	Given I am on main Page
	When I click on Hotele link
	And I have entered kraków into the city input
    And I have entered 16 wrzesień 2016 into the checkin date input
	And I have entered 26 wrzesień 2016 into the checkout date input
	When I hit search for hotel button
	Then Hotels should be displayed



Scenario: Proper error when searching for flyight with same departure/arrival
	Given I am on main Page
	And I have entered kraków into the  departure input
	And I have entered kraków into the  arrival input
	Then I get Twoja podróż jest zbyt krótka alert
	