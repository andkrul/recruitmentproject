﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eSkyTestProject.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace eSkyTestProject.Stories.Steps
{
    [Binding]
    [Scope(Tag = "servicesTests")]

    public class ServicesAutomation
    {
        private AirportAutocompleteSerivce _airportAutocompleteSerivce;
        [Given(@"I am api user from Poland")]
        public void IamAPIuserFromPoland()
        {
            _airportAutocompleteSerivce = new AirportAutocompleteSerivce("pl");    
        }

        [When(@"I make request with (.*) query string")]
        public void ImakeRequeest(string query)
        {
            _airportAutocompleteSerivce.MakeRequest(query);
        }

        [Then(@"All suggestion should contain query text")]
        public void IsSuggestionsValid()
        {
            Assert.IsTrue(_airportAutocompleteSerivce.IsSuggestionsValid());
        }
        [Then(@"I should recive (.*) suggestion")]
        public void IsValidAmmountOfSuggestionsReturned(int amount)
        {
            Assert.IsTrue(_airportAutocompleteSerivce.IsValidSuggestionReturned(amount));
        }
    }
}
