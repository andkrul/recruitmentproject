﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eSkyTestProject.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using TechTalk.SpecFlow;

namespace eSkyTestProject.Stories.Steps
{
    public class CommonSteps
    {
        [Binding]
        [Scope(Tag = "UITests")]
        public class BeforeAfterWebTest
        {
            private IWebDriver _driver;

            [BeforeScenario]
            public void WebTestSetup()
            {
                _driver = new ChromeDriver();
                _driver.Manage().Window.Maximize();
                ScenarioContext.Current.Set<IWebDriver>(_driver);
            }

           
            [Given(@"I am on main Page")]
            public void GivenIAmOnMainPage()
            {
                HomePage homePage = new HomePage(_driver).NavigateTo();
                ScenarioContext.Current.Set<HomePage>(homePage);
                homePage.CloseCookieInfoBar();
            }


            [AfterScenario]
            public void TearDown()
            {
              //  _driver.Quit();
            }
        }
    }
}
