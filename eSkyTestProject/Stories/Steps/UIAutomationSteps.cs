﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eSkyTestProject.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace eSkyTestProject.Stories.Steps
{
    [Binding]
    [Scope(Tag = "UITests")]
    public class UIAutomationSteps
    {

        private HomePage _homePage = ScenarioContext.Current.ContainsKey(typeof(HomePage).ToString()) ? ScenarioContext.Current.Get<HomePage>() : null;
        private CarsPage _carsPage;
        private HotelsPage _hotelsPage;
        private ContactPage  _contactPage ;

        [Given(@"I have entered (.*) into the  departure input")]
        public void GivenIHaveEnteredKrakowIntoTheDepartureInput(string city)
        {
            _homePage.SetDepartueInput(city);
        }

        [Given(@"I have entered (.*) into the  arrival input")]
        public void GivenIHaveEnteredBerlinIntoTheArrivalInput(string city)
        {
            _homePage.SetArrivalInput(city);
        }

        [Given(@"I have entered (.*) (.*) (.*) into the departure date input")]
        public void GivenIHaveEnteredWrzesienIntoTheDepartureDateInput(int day, string month, int year)
        {
            _homePage.ClickDepartureDateInput(day, month,year);
        }

        [Given(@"I have entered (.*) (.*) (.*) into the arrival date input")]
        public void GivenIHaveEnteredWrzesienIntoTheArrivalDateInput(int day, string month, int year)
        {
            _homePage.ClickArrivalDateInput(day, month, year);
        }


        [When(@"I have entered (.*) into the city input")]
        public void IhaveenteredIntoCityIput(string city)
        {
            _hotelsPage.SetCityInput(city);
        }
     



        [When(@"I have entered (.*) (.*) (.*) into the checkin date input")]
        public void GivenIHaveEnteredIntoCheckinDateInput(int day, string month, int year)
        {
            _hotelsPage.SetCheckInDate(day, month, year);
        }

        [When(@"I have entered (.*) (.*) (.*) into the checkout date input")]
        public void GivenIHaveEnteredIntoCheckOutDateInput(int day, string month, int year)
        {
            _hotelsPage.SetCheckOutDate(day, month, year);
        }
        [When(@"I hit search button")]
        public void WhenIHitSearchButton()
        {
            _homePage.ClickSearchButton();
        }

        [When(@"I hit search for hotel button")]
        public void WhenIHitSearchButtonHotel()
        {
            _hotelsPage.ClickSearchButton();
        }



        [When(@"I click on Samochody link")]
        public void WhenIClickOnSamochodyLink()
        {
            _carsPage = _homePage.GoToCarsPage();
        }

        [When(@"I chose Bagaż from left pane")]
        public void WhenIClickBagageLink()
        {
            _contactPage.ClickBagageLink();
        }

        [When(@"I chose  sausage from right pane")]
        public void WhenIClickSausageLink()
        {
            _contactPage.ClickSausageLink();
        }


        [When(@"I click on Hotele link")]
        public void WhenIClickOnHoteleLink()
        {
            _hotelsPage= _homePage.GoToHotelsPage();
        }



  
             [When(@"I click on Kontakt link")]
        public void WhenIClickKontatkLink()
        {
            _contactPage = _homePage.GotContactPage();
        }
        [When(@"I chose (.*) from country dropdown")]
        public void WhenIChosePolskaFromCountryDropdown(string country)
        {
            _carsPage.SelectFromCountryDrp(country);
        }

        [When(@"I chose (.*) from city dropdown")]
        public void WhenIChoseKrakowFromCityDropdown(string city)
        {
            _carsPage.SelectFromCityDrp(city);
        }



        [Then(@"Flight should be displayed")]
        public void ThenFlightShouldBeDisplayed()
        {
            Assert.IsTrue(_homePage.IsFlightsDisplayed());
        }

        [Then(@"I get (.*) alert")]
        public void IsValidAmmountOfSuggestionsReturned(string errorText)
        {
            Assert.IsTrue(_homePage.IsErrorDisplayed(errorText));
        }

        [Then(@"Hotels should be displayed")]
        public void ThenHotelsShouldBeDisplayed()
        {
            Assert.IsTrue(_hotelsPage.IsHotelsDisplayed());
        }


        [Then(@"I am on page with information about sausage")]
        public void ThenIAmOnPageWithInformationAboutSausage()
        {
           Assert.IsTrue(_contactPage.IsOnSausageSubPage());
        }


    }
}
