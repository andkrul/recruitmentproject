﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using eSkyTestProject.Models;
using Newtonsoft.Json;
using RestSharp;
using TechTalk.SpecFlow;

namespace eSkyTestProject.Services
{
    [Binding]
    class AirportAutocompleteSerivce
    {
        private readonly string _serviceUrl;
        private RestClient _client;
        private RestRequest _request;
        private AirPortModel _airPortModel;
        private string _query;
        public AirportAutocompleteSerivce(string countryCode)
        {
            _serviceUrl = EnvironmentVariables.AirporAutoCompleteSerivceURL;
            _client = new RestClient(_serviceUrl);
            _request = new RestRequest(Method.GET);
            _request.AddParameter("language", countryCode);
            _request.AddParameter("country", countryCode);
            _request.AddParameter("callback", "getAirports");
        }

        public AirPortModel MakeRequest(string query)
        {
            _query = query;
            _request.AddParameter("query", query);

            IRestResponse response = _client.Execute(_request);
            _airPortModel= GetAirPortFromRepsonse(response);
            return _airPortModel;
        }

        private static AirPortModel GetAirPortFromRepsonse(IRestResponse response)
        {
            string airPortJson = response.Content.Replace("getAirports(", "").Replace(");", "");
            return JsonConvert.DeserializeObject<AirPortModel>(airPortJson);
        }

        public bool IsSuggestionsValid()
        {
            return (_airPortModel.result.TrueForAll(e => e.suggestion.Contains(_query)));
        }

        public bool IsValidSuggestionReturned(int amount)
        {
            return _airPortModel.result.Count == 1;
        }
    }
}
