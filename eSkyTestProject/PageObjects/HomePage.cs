﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D2DLink.TireReplenishmentTool.Automation.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;

namespace eSkyTestProject.PageObjects
{
    public class HomePage : PageObjectBase
    {

#pragma warning disable 0169, 0649
        [FindsBy(How = How.Id, Using = "departureRoundtrip0")]
        private IWebElement _departureInput;
        [FindsBy(How = How.Id, Using = "arrivalRoundtrip0")]
        private IWebElement _arivvalInput;
        [FindsBy(How = How.Id, Using = "departureDateRoundtrip0")]
        private IWebElement _departureDateInput;
        [FindsBy(How = How.Id, Using = "departureDateRoundtrip1")]
        private IWebElement _arrivalDateInput;
        [FindsBy(How = How.ClassName, Using = "ui-datepicker-next")]
        private IWebElement _nextMonthBtn;
        [FindsBy(How = How.ClassName, Using = "ui-datepicker-year")]
        private IWebElement _selectetYear;
        [FindsBy(How = How.ClassName, Using = "ui-datepicker-month")]
        private IWebElement _selectedMonth;
        [FindsBy(How = How.XPath, Using = @"//button[@class='btn transaction qsf-search']")]
        private IWebElement _searchBtn;
        [FindsBy(How = How.LinkText, Using = "Samochody")]
        private IWebElement _carsLnk;
        [FindsBy(How = How.LinkText, Using = "Hotele")]
        private IWebElement _HotelsLnk;
        [FindsBy(How = How.LinkText, Using = "Kontakt")]
        private IWebElement _contaktLnk;
        [FindsBy(How = How.ClassName, Using = "error-msg")]
        private IWebElement _errorMsg;
        [FindsBy(How = How.ClassName, Using = "cookie-info-close")]
        private IWebElement _cokiesInfoCloseBtn;

#pragma warning restore 0169, 0649


        public HomePage(IWebDriver driver) : base(driver)
        {
        }

        public HomePage SetDepartueInput(string city)
        {
            _departureInput.SendKeys(city);
            return this;
        }

        public void SetArrivalInput(string city)
        {
            _arivvalInput.SendKeys(city);
        }

        public void ClickDepartureDateInput(int day, string month, int year)
        {
            _departureDateInput.Click();
            Calendar calendar = new Calendar(Driver.FindElement(By.Id("ui-datepicker-div")));
            calendar.setDate(day, month, year);
        }

        public void ClickArrivalDateInput(int day, string month, int year)
        {
            _arrivalDateInput.Click();
            Calendar calendar = new Calendar(Driver.FindElement(By.Id("ui-datepicker-div")));
            calendar.setDate(day, month, year);
        }

        public HomePage NavigateTo()
        {
            Driver.Navigate().GoToUrl(EnvironmentVariables.BaseURL);
            return this;
        }


        public void ClickSearchButton()
        {
            _searchBtn.Click();
        }

        public CarsPage GoToCarsPage()
        {
            _carsLnk.Click();
            return new CarsPage(Driver);
        }

        public HotelsPage GoToHotelsPage()
        {
            _HotelsLnk.Click();
            return new HotelsPage(Driver);
        }
        public ContactPage GotContactPage()
        {
            _contaktLnk.Click();
            return new ContactPage(Driver);
        }

        public  bool IsErrorDisplayed(string errorText)
        {
            Driver.WaitUntil(x => x.FindElement(By.ClassName("error-msg")));
            return _errorMsg.Text.Contains(errorText);
        }

        public bool IsFlightsDisplayed()
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(25));
            Driver.WaitUntil(x => x.FindElement(By.ClassName("search-results")).Displayed);
            return true;
        }

        public void CloseCookieInfoBar()
        {
            _cokiesInfoCloseBtn.Click();
        }
    }


}
