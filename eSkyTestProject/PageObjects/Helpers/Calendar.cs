﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace eSkyTestProject.PageObjects
{
    public class Calendar
    {
        private IWebElement _calendar;
        public Calendar(IWebElement calendar)
        {
            _calendar = calendar;
        }


        public void setDate(int day, string month, int year)
        {
            while (!_calendar.FindElement(By.ClassName("ui-datepicker-month")).Text.Equals(month))
            {
                _calendar.FindElement(By.ClassName("ui-datepicker-next")).Click();
            }
            _calendar.FindElement(By.XPath($@"//a[@class='ui-state-default'  and text()='{day}']")).Click();
        }
    }
}
