﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace eSkyTestProject.PageObjects
{
    public abstract class PageObjectBase
    {
        protected readonly IWebDriver Driver;

        protected PageObjectBase(IWebDriver driver)
        {
            Driver = driver;
            PageFactory.InitElements(driver, this);
        }


    }
}
