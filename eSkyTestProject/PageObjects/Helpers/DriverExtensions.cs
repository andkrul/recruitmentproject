﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace D2DLink.TireReplenishmentTool.Automation.PageObjects
{
    public static class DriverExtensions
    {

   

        public static void WaitForObject(this IWebDriver driver)
        {
            DateTime datetime = DateTime.Now;
            bool success = false;
            while (DateTime.Now < datetime.AddSeconds(45))// Timeout hard-coded to 45 seconds for now
            {
                try
                {
                    var ajaxIsComplete =
                        // ReSharper disable once PossibleNullReferenceException
                        (bool)((IJavaScriptExecutor)driver).ExecuteScript("return jQuery.active == 0");
                    bool documentReady =
                        // ReSharper disable once PossibleNullReferenceException
                        (driver as IJavaScriptExecutor).ExecuteScript("return document.readyState").Equals("complete");
                    if (ajaxIsComplete && documentReady)
                    {
                        success = true;
                        break;
                    }
                    Thread.Sleep(100);
                }
                catch (InvalidOperationException e)
                {
                    if (e.Message == "JavaScript error (UnexpectedJavaScriptError)")
                        Debug.WriteLine("Page not yet ready.");
                    else
                        throw;
                }
            }
            if (!success)
                throw new Exception("Timed out waiting for ajax to complete.");
        }

        public static T WaitUntil<T>(this IWebDriver browser, Func<IWebDriver, T> condition, int timeout = 25)
        {
            var wait = new WebDriverWait(browser, new TimeSpan(0, 0, timeout));
            return wait.Until(condition);
        }

        public static void WaitForAnimation(this IWebDriver driver)
        {
            DateTime datetime = DateTime.Now;
            bool success = false;
            while (DateTime.Now < datetime.AddSeconds(15)) // Timeout hard-coded to 15 seconds for now
            {
                try
                {
                    int numberOfAnimations;
                    // ReSharper disable once PossibleNullReferenceException
                    bool parsed =
                        int.TryParse((driver as IJavaScriptExecutor).ExecuteScript("return $(':animated').length").ToString(), out numberOfAnimations);

                    if (parsed && (numberOfAnimations == 0))
                    {
                        success = true;
                        break;
                    }
                    Thread.Sleep(100);
                }
                catch (InvalidOperationException e)
                {
                    if (e.Message == "JavaScript error (UnexpectedJavaScriptError)")
                        Debug.WriteLine("Page not yet ready.");
                    else
                        throw;
                }
            }
            if (!success)
                throw new Exception("Timed out waiting for animations to complete.");
        }

        public static bool IsElementDisplayed(this IWebDriver driver, By locator)
        {
            return driver.FindElements(locator).Any();
        }

        public static void ClickOnElemntUsingJs(this IWebDriver driver, By locator)
        {
            var element = driver.FindElement(locator);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public static void ClickOnElemntUsingJs(this IWebDriver driver, IWebElement element)
        {
            IJavaScriptExecutor executor = (IJavaScriptExecutor)driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public static void DoubleClickOnElement(this IWebDriver driver, By locator)
        {
            var action = new Actions(driver);
            action.DoubleClick(driver.FindElement(locator)).Perform();
        }

        public static void DoubleClickOnElement(this IWebDriver driver, IWebElement element)
        {
            var action = new Actions(driver);
            action.DoubleClick(element).Perform();
        }
        public static bool IsElementDisplayed(this IWebElement element, By locator)
        {
            try
            {
                element.FindElement(locator);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

    }
}
