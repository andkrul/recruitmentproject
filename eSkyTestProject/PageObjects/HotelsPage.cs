﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D2DLink.TireReplenishmentTool.Automation.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace eSkyTestProject.PageObjects
{
    public class HotelsPage : PageObjectBase
    {
        [FindsBy(How = How.Id, Using = "City")]
        private IWebElement _cityInput;
        [FindsBy(How = How.Id, Using = "CheckInDate")]
        private IWebElement _checkInDate;
        [FindsBy(How = How.Id, Using = "CheckOutDate")]
        private IWebElement _checkOutDate;
        [FindsBy(How = How.XPath, Using = @"//button[@type='submit']")]
        private IWebElement _searchBtn;
        
        public HotelsPage(IWebDriver driver) : base(driver)
        {
        }

        public void SetCityInput(string country)
        {
            _cityInput.SendKeys(country);
        }

        public void SetCheckInDate(int day,string month,int year)
        {

            _checkInDate.Click();
            Calendar calendar = new Calendar(Driver.FindElement(By.Id("ui-datepicker-div")));
            calendar.setDate(day, month, year);
        }

        public void SetCheckOutDate(int day, string month, int year)
        {
            _checkOutDate.Click();
            Calendar calendar = new Calendar(Driver.FindElement(By.Id("ui-datepicker-div")));
            calendar.setDate(day, month, year);
        }

        public void ClickSearchButton()
        {
            _searchBtn.Click();
        }

        public bool IsHotelsDisplayed()
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(15));
            return (Driver.FindElement(By.XPath("//ul[@class='hotels-result-list']"))).Displayed;

        }
    }
}
