﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D2DLink.TireReplenishmentTool.Automation.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace eSkyTestProject.PageObjects
{
    public class ContactPage : PageObjectBase
    {

        
       
        [FindsBy(How = How.PartialLinkText, Using = "kiełbasy")]
        private IWebElement _suasageLink;
        public ContactPage(IWebDriver driver) : base(driver)
        {

        }

        public void ClickBagageLink()
        {
            var categories = Driver.FindElements(By.ClassName("category-name"));
            categories.FirstOrDefault(x=>x.Text.Contains("Bagaż")).Click();
        }
        public void ClickSausageLink()
        {
                Driver.ClickOnElemntUsingJs(_suasageLink);          
        }

        public bool IsOnSausageSubPage()
        {
            return  Driver.FindElement(By.ClassName("article-title")).Text.Contains("Kiełbasa");
        }
    }
}
