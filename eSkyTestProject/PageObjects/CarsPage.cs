﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace eSkyTestProject.PageObjects
{
    public class CarsPage : PageObjectBase
    {
        [FindsBy(How = How.Id, Using = "pu-country")]
        private IWebElement _countryDrp;
        [FindsBy(How = How.Id, Using = "pu-city")]
        private IWebElement _cityDrp;
        [FindsBy(How = How.Id, Using = "formsubmit")]
        private IWebElement _searchBtn;

        
        public CarsPage(IWebDriver driver) : base(driver)
        {
        }

        public void SelectFromCountryDrp(string country)
        {
            _countryDrp.SendKeys(country);
        }

        public void SelectFromCityDrp(string city)
        {
            _cityDrp.SendKeys(city);
        }

        public void ClickSearchBtn()
        {
            _searchBtn.Click();
        }
    }
}
